;; Based on https://www.emacswiki.org/emacs/ModeTutorial

(defvar st-mode-hook nil)

(defvar st-mode-map
  (let ((map (make-keymap)))
    (define-key map "\C-j" 'newline-and-indent)
    map)
  "Keymap for Smalltalk major mode")

(defconst st-font-lock-keywords-pragmas
  (list
   '("<\\w+:\\s-*.*?>" . font-lock-preprocessor-face)
   )
  "Highlighting in ST mode")

;; First, remove comments, including #! line
(defconst st-font-lock-keywords-comments
  (list
   '("\\`#!.*" . font-lock-comment-face)
   '("\"[^\"]*?\"" . font-lock-comment-face)
   )
  "Font lock comments"
  )

(defconst st-font-lock-keywords-strings
  (list
   '("'[^']*?'" (0 font-lock-string-face))
   )
  "Font lock strings"
  )

(defconst st-font-lock-keywords-constants
  (list
   '("#[a-zA-Z]\\w*" (0 font-lock-constant-face))
   '("\\([a-zA-Z][a-zA-Z0-9]*:\\)\s-+\\([a-zA-Z][a-zA-Z0-9]*\\)"
     (1 font-lock-keyword-face)
     (2 font-lock-constant-name-face)) ;; Parameters
   '("\\([0-9]+r\\)?-?[0-9]+\\(\\.[0-9]+\\)?\\(\\(d\\|e\\|q\\|s\\)-?[0-9]+\\)?" (0 font-lock-constant-face))
   '("\\(#(\\).*?\\()\\)"
     (1 font-lock-constant-face t)
     (2 font-lock-constant-face t)
     )
   '("\\(#\\[\\).*?\\(\\]\\)"
     (1 font-lock-constant-face)
     (2 font-lock-constant-face)
     )
   '("\\(#{\\).*?\\(}\\)"
     (1 font-lock-constant-face)
     (2 font-lock-constant-face)
     )
   )
  "Highlighting in ST mode")

(defconst st-font-lock-keywords-punctuation
  (list
   '("[!.;^]" . font-lock-negation-char-face)
   )
  "Additional Keywords to highlight in ST mode")

(defconst st-font-lock-keywords-reserved
  (list
   '("\\(?:false\\|nil\\|s\\(?:elf\\|uper\\)\\|t\\(?:hisContext\\|rue\\)\\)" . font-lock-constant-face) ;; reserved words
   )
  "Highlighting in ST mode")

;;Then mark all the built-in GNU Smalltalk Classes
(defconst st-font-lock-keywords-builtins
  (list
   '("\\<\\(?:A\\(?:lternativeObjectProxy\\|r\\(?:ithmeticError\\|ray\\(?:edCollection\\)?\\)\\|ssociation\\)\\|B\\(?:ag\\|ehavior\\|lockC\\(?:losure\\|ontext\\)\\|oolean\\|rowser\\|yte\\(?:Array\\|Stream\\)\\)\\|C\\(?:A\\(?:ggregate\\|rray\\(?:CType\\)?\\)\\|B\\(?:oolean\\|yte\\)\\|C\\(?:har\\|ompound\\)\\|Double\\|F\\(?:loat\\|unctionDescriptor\\)\\|Int\\|Long\\|Object\\|Ptr\\(?:CType\\)?\\|S\\(?:calar\\(?:CType\\)?\\|hort\\|malltalk\\|tr\\(?:ing\\|uct\\)\\)\\|Type\\|U\\(?:Char\\|Int\\|Long\\|Short\\|nion\\)\\|haracter\\(?:Array\\)?\\|lass\\(?:Description\\)?\\|o\\(?:llection\\|mpiled\\(?:Block\\|Code\\|Method\\)\\|ntextPart\\|reException\\)\\)\\|D\\(?:LD\\|ate\\|elay\\(?:edAdaptor\\)?\\|i\\(?:ctionary\\|rect\\(?:edMessage\\|ory\\)\\)\\|umperProxy\\)\\|E\\(?:rror\\|xception\\(?:Collection\\)?\\)\\|F\\(?:alse\\|ile\\(?:S\\(?:egment\\|tream\\)\\)?\\|loat\\|raction\\)\\|Ha\\(?:lt\\|shedCollection\\)\\|I\\(?:dentity\\(?:Dictionary\\|Set\\)\\|nte\\(?:ger\\|rval\\)\\)\\|L\\(?:arge\\(?:Array\\(?:Subpart\\|edCollection\\)?\\|ByteArray\\|Integer\\|NegativeInteger\\|PositiveInteger\\|WordArray\\|ZeroInteger\\)\\|ink\\(?:edList\\)?\\)\\|M\\(?:a\\(?:gnitude\\|ppedCollection\\)\\|e\\(?:mory\\|ssage\\(?:NotUnderstood\\)?\\|t\\(?:aclass\\|hod\\(?:Context\\|Dictionary\\|Info\\)\\)\\)\\)\\|N\\(?:amespace\\|otification\\|u\\(?:ll\\(?:Proxy\\|ValueHolder\\)\\|mber\\)\\)\\|O\\(?:bject\\(?:Dumper\\)?\\|rderedCollection\\)\\|P\\(?:ackageLoader\\|luggable\\(?:Adaptor\\|Proxy\\)\\|o\\(?:int\\|sitionableStream\\)\\|rocess\\(?:orScheduler\\)?\\)\\|R\\(?:andom\\|e\\(?:ad\\(?:\\(?:Write\\)?Stream\\)\\|ctangle\\)\\|ootNamespace\\|unArray\\)\\|S\\(?:e\\(?:maphore\\|quenceableCollection\\|t\\)\\|haredQueue\\|ignal\\|mallInteger\\|ortedCollection\\|tr\\(?:eam\\|ing\\)\\|y\\(?:m\\(?:Link\\|bol\\)\\|stemDictionary\\)\\)\\|T\\(?:extCollector\\|ime\\|okenStream\\|r\\(?:appableEvent\\|ue\\)\\)\\|U\\(?:ndefinedObject\\|serBreak\\)\\|V\\(?:alue\\(?:\\(?:Adapto\\|Holde\\)r\\)\\|ersionableObjectProxy\\)\\|W\\(?:arning\\|ordArray\\|riteStream\\)\\|ZeroDivide\\)\\>" . font-lock-builtin-face) ; pre-defined classes
   )

  "Font lock built-in classes")

(defconst st-font-lock-keywords-variables
  (list
   '("|.+?|" . font-lock-variable-name-face) ;; Locals
   '("\\<\\([a-zA-Z][a-zA-Z0-9]*\\)\\s-*\\(:=\\)"
     ( 1 font-lock-variable-name-face) ;; Assignments
     ( 2 font-lock-keyword-face))
   '("\\[\\s-*\\(\\(:[a-zA-Z][a-zA-Z0-9]*\\s-*\\)+\\)|" (1 font-lock-variable-name-face)) ;; Block Locals
   )
  "Additional Keywords to highlight in ST mode")

(defconst st-font-lock-keywords-user-types
  (list
   '("\\b[A-Z]+[a-z0-9][a-zA-Z0-9]*\\b" . font-lock-type-face)
   )
  "Additional Keywords to highlight in ST mode")

(defconst st-font-lock-keywords-functions
  (list
   '("^\\s-*\\([a-zA-Z][a-zA-Z0-9]*\\)\\s-*\\[" 1 font-lock-function-name-face)
   '("^\\s-*\\([a-zA-Z][a-zA-Z0-9]*:\\)\\([^][]*\\)\\["
     (1 font-lock-function-name-face)
     ("\\([a-zA-Z][a-zA-Z0-9]*:\\)\s-*\\([a-zA-Z][a-zA-Z0-9]*\\)"
      (progn (goto-char (match-beginning 0)) ;; rewind
             (save-excursion
               (goto-char (match-end 0)) ;; Find end
               (point)))
      (goto-char (+ 1 (match-end 0)))  ;; Continue from end of match
      (1 font-lock-function-name-face)
      (2 font-lock-constant-face)
      )
     )
   '("^\\s-*\\(\\s_\\s_?\\)\\s-+\\([a-zA-Z][a-zA-Z0-9]*\\)\\s-*\\["
     (1 font-lock-function-name-face)
     (2 font-lock-variable-name-face))
   )
  "Additional Keywords to highlight in ST mode")

(defconst st-font-lock-keywords-keywords
  (list
   '(":=" . font-lock-keyword-face)
   '("[a-zA-Z][a-zA-Z0-9]*:" . font-lock-keyword-face)
   '("\\s_\\s_?\\S_" . font-lock-keyword-face)
   )
  "Additional Keywords to highlight in ST mode")

(defvar st-font-lock-keywords (append
                               st-font-lock-keywords-pragmas
                               st-font-lock-keywords-comments
                               st-font-lock-keywords-strings
                               st-font-lock-keywords-constants
                               st-font-lock-keywords-punctuation
                               st-font-lock-keywords-reserved
                               st-font-lock-keywords-variables
                               st-font-lock-keywords-builtins
                               st-font-lock-keywords-user-types
                               st-font-lock-keywords-functions
                               st-font-lock-keywords-keywords)
 "Default highlighting expressions for Smalltalk mode")

(defun st-indent-line ()
  "Indent current line as Smalltalk code"
  (interactive)
  (beginning-of-line)
  (if (bobp)  ; Check for rule 1
      (indent-line-to 0)
    (let ((prev-indent 0)
          (cur-indent 0))
      (save-excursion
        (forward-line -1)
        (while (and
                (looking-at "^[ \t]*$")
                (not (bobp)))
          (forward-line -1)
          )
        (setq cur-indent (+ cur-indent (st-net-indent)))
;;        (message "%s" (what-line))
        (when (looking-at "^[^\\s(\\r\\n]*\\s)")
          (setq cur-indent (+ cur-indent (st-count-leading-closes)))
;;          (message "%s" "Starts with ]")
          )
        (beginning-of-line)
        (setq prev-indent (current-indentation))
        ) ;; Back to current line
      (save-excursion
        (when (looking-at "^[^\\s(\\n\\r]*\\s)")
          (setq cur-indent (- cur-indent (st-count-leading-closes)))
;;          (message "%s" "I start with ]")
          )
        )
;;      (message "%s" cur-indent)

      (setq cur-indent (+ prev-indent (* cur-indent tab-width)))

      (if (< cur-indent 0)
          (setq cur-indent 0))

      (if cur-indent
          (indent-line-to cur-indent)
        (indent-line-to 0)))))
                                        ; If we didn't see an indentation hint, then allow no indentation

(defun st-count-leading-closes ()
  "Count the number of ] at the start of a line"
  (save-excursion
    (count-matches "^\\([ \t]*\\s)\\)*" (progn (beginning-of-line) (point)) (progn (end-of-line) (point)))))

(defun st-count-opens ()
  "Count the number of [ on a line"
  (save-excursion
    (count-matches "\\s(" (progn (beginning-of-line) (point)) (progn (end-of-line) (point)))))

(defun st-count-closes ()
  "Count the number of ] on a line"
  (interactive)
  (save-excursion
    (count-matches "\\s)" (progn (beginning-of-line) (point)) (progn (end-of-line) (point)))))

(defun st-testing()
  "Testing"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (if (looking-at "^[^[\\r\\n]+\\]")
        (message "%s" "Yes!")
      (message "%s" "Nope"))))

(defun st-net-indent ()
  (- (st-count-opens) (st-count-closes)))

(defvar st-mode-syntax-table
  (let ((table (make-syntax-table)))
        ;; Make sure A-z0-9 are set to "w   " for completeness
    (let ((c 0))
      (setq c ?0)
      (while (<= c ?9)
	(setq c (1+ c))
	(modify-syntax-entry c "w   " table))
      (setq c ?A)
      (while (<= c ?Z)
	(setq c (1+ c))
	(modify-syntax-entry c "w   " table))
      (setq c ?a)
      (while (<= c ?z)
	(setq c (1+ c))
	(modify-syntax-entry c "w   " table)))
;;    (modify-syntax-entry 10  "-   " table) ; newline
    (modify-syntax-entry ?:  ".   " table) ; Symbol-char
    (modify-syntax-entry ?_  "w   " table) ; Symbol-char
    (modify-syntax-entry ?\" "!1  " table) ; Comment (generic)
    (modify-syntax-entry ?'  "\"  " table) ; String
    (modify-syntax-entry ?#  "'   " table) ; Symbol or Array constant
    (modify-syntax-entry ?\( "()  " table) ; Grouping
    (modify-syntax-entry ?\) ")(  " table) ; Grouping
    (modify-syntax-entry ?\[ "(]  " table) ; Block-open
    (modify-syntax-entry ?\] ")[  " table) ; Block-close
    (modify-syntax-entry ?{  "(}  " table) ; Array-open
    (modify-syntax-entry ?}  "){  " table) ; Array-close
    (modify-syntax-entry ?$  "/   " table) ; Character literal
    (modify-syntax-entry ?!  ".   " table) ; End message / Delimit defs
    (modify-syntax-entry ?\; ".   " table) ; Cascade
    (modify-syntax-entry ?|  ".   " table) ; Temporaries
    (modify-syntax-entry ?^  ".   " table) ; Return
    (modify-syntax-entry ?.  ".   " table) ; End
    ;; Just to make sure these are not set to "w   "
    (modify-syntax-entry ?<  "_   " table)
    (modify-syntax-entry ?>  "_   " table)
    (modify-syntax-entry ?+  "_   " table) ; math
    (modify-syntax-entry ?-  "_   " table) ; math
    (modify-syntax-entry ?*  "_   " table) ; math
    (modify-syntax-entry ?/  "_  " table) ; math
    (modify-syntax-entry ?=  "_   " table) ; bool/assign
    (modify-syntax-entry ?%  "_   " table) ; valid selector
    (modify-syntax-entry ?&  "_   " table) ; boolean
    (modify-syntax-entry ?\\ "_   " table) ; ???
    (modify-syntax-entry ?~  "_   " table) ; misc. selector
    (modify-syntax-entry ?@  "_   " table) ; Point
    (modify-syntax-entry ?,  "_   " table) ; concat
    table)
  "Syntax table used by Smalltalk mode")

(define-derived-mode st-mode prog-mode
  "Major mode for editing Workflow Process Description Language files"
  (kill-all-local-variables)
  (set-syntax-table st-mode-syntax-table)
  (use-local-map st-mode-map)
  (set (make-local-variable 'tab-width) 4)

  (setq font-lock-defaults '(st-font-lock-keywords t))
  (set (make-local-variable 'comment-start) "\"")
  (set (make-local-variable 'comment-end) "\"")
  (set (make-local-variable 'indent-line-function) 'st-indent-line)
  (setq font-lock-multiline t)

  (setq major-mode 'st-mode)
  (setq mode-name "ST")
  (run-hooks 'prog-mode-hook)
  (run-hooks 'st-mode-hook))

(add-to-list 'hs-special-modes-alist
             '(st-mode "\\[" "\\]" "\"" nil nil))


(provide 'st-mode)
